# Nature Trail

Nature Trail Theme is Mobile-friendly Drupal 9 and 10 responsive theme. This theme 
features a custom Slider, responsive layout, multiple column layouts and is highly 
customizable. It also supports font awesome, and it is great for any kind of Nature 
website.

For a full description of the theme, visit the
[project page](https://www.drupal.org/project/nature_trail).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/nature_trail).


## Table of contents

- Requirements
- Installation
- Configuration
- Features
- Maintainers


## Requirements

This theme requires no modules outside of Drupal core.


## Installation

1. Move the theme directory to your Drupal installation:
   **mv nature_trail/path/to/drupal/themes/**
2. Log in to your Drupal site as an administrator.
3. Go to the Appearance page (`/admin/appearance`).
4. Find the Nature Trail Theme and click on the "Install and set as default"
   button.
5. Configure the theme settings according to your preferences.


## Configuration

 Go to Administration » Appearance » Nature Trail » Settings
  - Change the background Slider image.
  - Hide/show and change copyright text.
  - Hide/show social icons in the footer.


## Features

1. Responsive, Mobile-Friendly Theme
2. In built Font Awesome
3. Mobile support (Smartphone, Tablet, Android, iPhone, etc)
4. A total of 12 block regions
5. Custom Slider with background Image
6. Sticky header
7. Drop Down main menus with toggle menu at mobile.


## Maintainers

- Bhupendra_Raykhere - [bhupendra_raykhere](https://www.drupal.org/u/bhupendra_raykhere)
