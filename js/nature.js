(function ($, Drupal, once) {
    Drupal.behaviors.CustomJS = {
        attach: function (context, settings) {
            jQuery(' .toolbar-horizontal .slider-section').css('height', jQuery(window).height());
            // sticky header
            jQuery(window).scroll(function () { // this will work when your window scrolled.
                var height = jQuery(window).scrollTop();
                if (height > 100) {
                    jQuery(".header").addClass("fixed-top");
                    jQuery(".header").removeClass("main-header");
                } else {
                    jQuery(".header").removeClass("fixed-top");
                    jQuery(".header").addClass("main-header");
                }
            });

            jQuery(window).resize(function () {
                jQuery('.slider-section').css('height', jQuery(window).height());
            });
        },
    };
}(jQuery, Drupal, once));