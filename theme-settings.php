<?php

/**
 * @file
 * Danland theme settings.
 */

use Drupal\file\Entity\File;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */

function nature_trail_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
   // Slide Show
   $form['nature_trail_info'] = [
    '#markup' => '<h2><br/>Advanced theme settings</h2><div class="messages messages--warning">Clear cache after making any changes in theme settings. <a href="../../config/development/performance">Click here to clear cache</a></div>',
  ];
  $form['nature_trail_settings']['slideshow'] = [
    '#type' => 'details',
    '#title' => t('Front Page Slider.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['nature_trail_settings']['slideshow']['no_of_slides'] = [
    '#type' => 'textfield',
    '#title' => t('Number of slides'),
    '#default_value' => theme_get_setting('no_of_slides'),
    '#description'  => t("Enter the number of slides required & Save configuration."),
    '#markup' => '<div class="messages messages--warning">Clear caches after making any changes in theme settings. <a href="../../config/development/performance">Click here to clear cache</a></div>',
  ];
  $form['nature_trail_settings']['slideshow']['show_slideshow'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Slideshow'),
    '#default_value' => theme_get_setting('show_slideshow'),
    '#description'   => t("Show/hide slideshow in home page."),
  ];
  $form['nature_trail_settings']['slideshow']['slide'] = [
    '#markup' => t('Change the banner image, title, description and link using below fieldset.'),
  ];
  for ($i = 1; $i <= theme_get_setting('no_of_slides'); $i++) {
    $form['nature_trail_settings']['slideshow']['slide' . $i] = [
      '#type' => 'details',
      '#title' => t('Slide ' . $i),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['nature_trail_settings']['slideshow']['slide' . $i]['slide_image' . $i] = [
      '#type' => 'managed_file',
      '#title' => t('Slide ' . $i . ' Image'),
      '#default_value' => theme_get_setting('slide_image' . $i, 'nature_trail'),
      '#upload_location' => 'public://',
    ];
    
    $form['nature_trail_settings']['slideshow']['slide' . $i]['slide_title_' . $i] = [
      '#type' => 'textfield',
      '#title' => t('Slide ' . $i . ' Title'),
      '#default_value' => theme_get_setting('slide_title_' . $i, 'nature_trail'),
    ];
    $form['nature_trail_settings']['slideshow']['slide' . $i]['slide_description_' . $i] = [
      '#type' => 'textarea',
      '#title' => t('Slide ' . $i . ' Description'),
      '#default_value' => theme_get_setting('slide_description_' . $i, 'nature_trail'),
    ];
    $form['nature_trail_settings']['slideshow']['slide' . $i]['slide_url_' . $i] = [
      '#type' => 'textfield',
      '#title' => t('Slide ' . $i . ' URL'),
      '#default_value' => theme_get_setting('slide_url_' . $i, 'nature_trail'),
    ];
    $form['nature_trail_settings']['slideshow']['slide' . $i]['slide_url_text' . $i] = [
      '#type' => 'textfield',
      '#title' => t('Slide ' . $i . ' URL Text'),
      '#default_value' => theme_get_setting('slide_url_text' . $i, 'nature_trail'),
    ];
    // Footer Social Icon Link.
    $form['nature_trail_settings']['social_share_icon'] = [
      '#type' => 'details',
      '#title' => t('Social Icons Links'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['nature_trail_settings']['social_share_icon']['show_social_icons'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Social Icons'),
      '#default_value' => theme_get_setting('show_social_icons'),
      '#description'   => t("Show/hide Social media links."),
    ];
    $form['nature_trail_settings']['social_share_icon']['twitter_url'] = [
      '#type' => 'textfield',
      '#title' => t('Twitter URL'),
      '#default_value' => theme_get_setting('twitter_url'),
    ];
    $form['nature_trail_settings']['social_share_icon']['facebook_url'] = [
      '#type' => 'textfield',
      '#title' => t('Facebook URL'),
      '#default_value' => theme_get_setting('facebook_url'),
    ];
    $form['nature_trail_settings']['social_share_icon']['instagram_url'] = [
      '#type' => 'textfield',
      '#title' => t('Instagram URL'),
      '#default_value' => theme_get_setting('instagram_url'),
    ];
    $form['nature_trail_settings']['social_share_icon']['linkedin_url'] = [
      '#type' => 'textfield',
      '#title' => t('LinkedIn URL'),
      '#default_value' => theme_get_setting('linkedin_url'),
    ];
    $form['nature_trail_settings']['copyright'] = [
      '#type' => 'details',
      '#title' => t('Copyright'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['nature_trail_settings']['copyright']['show_copyright'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Copyright text'),
      '#default_value' => theme_get_setting('show_copyright'),
      '#description'   => t("Check this option to show Copyright text. Uncheck to hide."),
    ];
  
    $form['nature_trail_settings']['copyright']['copyright_text'] = [
      '#type' => 'textfield',
      '#title' => t('Enter copyright text'),
      '#default_value' => check_markup(theme_get_setting('copyright_text'), 'full_html')
    ];

    //Make the uploaded images permanent.
    $img_slide = theme_get_setting('slide_image'.$i,'nature_trail');
    if(!empty($img_slide)){
      $file = File::load($img_slide[0]);
      $file->setPermanent();
      $file->save();
      $file_usage = \Drupal::service('file.usage');
      $file_usage_check = $file_usage->listUsage($file);
      if (empty($file_usage_check)) {
        $file_usage->add($file, 'nature_trail', 'theme', $img_slide[0]);
      }
    }
}

}